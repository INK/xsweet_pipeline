require 'xsweet_pipeline/docx_to_html_extract_step'
require 'xsweet_pipeline/download_and_execute_xsl_via_saxon_on_docx'
require 'ostruct'
require 'spec_helper'

describe XsweetPipeline::DownloadAndExecuteXslViaSaxonOnDocx do

  let(:process_step)  { double(:process_step) }
  subject             { XsweetPipeline::DownloadAndExecuteXslViaSaxonOnDocx.new(process_step: process_step) }

  describe '#perform_step' do
    context 'when converting a HTML file' do

      let(:input_file)            { File.new('spec/fixtures/files/basic_doc.docx', 'r') }
      let(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/docx-html-extract.xsl') }
      let(:remote_uri)            { "https://gitlab.coko.foundation/wendell/XSweet/raw/ink-api-publish/docx-html-extract.xsl" }

      before do
        stub_request(:get, remote_uri).
            with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
            to_return(:status => 200, :body => xsl_file, :headers => {})
      end

      it 'should return a result for the first test document' do
        result = subject.perform_step(files: input_file.path, options: {remote_xsl_uri: remote_uri})

        expect(result).to_not be_nil
        expect(File.read(result)).to_not eq input_file
        expect(File.read(result)).to eq File.read('spec/fixtures/files/xsweet_1_extract_result.html')
      end
    end
  end

  describe '#version' do
    specify do
      expect{subject.version}.to_not raise_error
    end
  end
end