require 'webmock/rspec'
require 'httparty'

RSpec.configure do |config|
  WebMock.disable_net_connect!(allow_localhost: true)
end