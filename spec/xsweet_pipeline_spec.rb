require 'spec_helper'

require 'xsweet_pipeline/download_and_execute_xsl_via_saxon'
require 'xsweet_pipeline/handle_notes_step'
require 'xsweet_pipeline/scrub_step'
require 'xsweet_pipeline/join_elements_step'
require 'xsweet_pipeline/zorba_map_step'

describe XsweetPipeline::DownloadAndExecuteXslViaSaxon do
  let(:process_step)  { double(:process_step) }
  subject             { XsweetPipeline::DownloadAndExecuteXslViaSaxon.new(process_step: process_step) }
  
  def test_result(file:, result_path:)
    result = subject.perform_step(files: file.path)

    expect(result).to_not be_nil
    expect(File.read(result)).to_not eq file
    expect(File.read(result)).to eq File.read(result_path)
  end

  describe '#perform_step' do

    let(:remote_uri)           { subject.remote_xsl_location }

    before do
      stub_request(:get, remote_uri).
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
          to_return(:status => 200, :body => xsl_file, :headers => {})
    end

    describe 'Xsweet pipeline step 1' do
      subject                      { XsweetPipeline::DocxToHtmlExtractStep.new(process_step: process_step) }
      let!(:html_file)             { File.new('spec/fixtures/files/basic_doc.docx', 'r') }
      let!(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/docx-html-extract.xsl') }

      specify do
        test_result(file: html_file, result_path: 'spec/fixtures/files/xsweet_1_extract_result.html')
      end
    end


    describe 'Xsweet pipeline step 2' do
      subject                      { XsweetPipeline::HandleNotesStep.new(process_step: process_step) }
      let!(:html_file)             { File.new('spec/fixtures/files/xsweet_2_handle_notes_input.html', 'r') }
      let!(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/handle-notes.xsl') }

      specify do
        test_result(file: html_file, result_path: 'spec/fixtures/files/xsweet_2_handle_notes_result.html')
      end
    end

    describe 'Xsweet pipeline step 3' do
      subject                     { XsweetPipeline::ScrubStep.new(process_step: process_step) }
      let!(:html_file)            { File.new('spec/fixtures/files/xsweet_3_scrub_input.html', 'r') }
      let(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/scrub.xsl') }

      specify do
        test_result(file: html_file, result_path: 'spec/fixtures/files/xsweet_3_scrub_result.html')
      end
    end

    describe 'Xsweet pipeline step 4' do
      subject                     { XsweetPipeline::JoinElementsStep.new(process_step: process_step) }
      let!(:html_file)            { File.new('spec/fixtures/files/xsweet_4_join_elements_input.html', 'r') }
      let(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/join-elements.xsl') }

      specify do
        test_result(file: html_file, result_path: 'spec/fixtures/files/xsweet_4_join_elements_result.html')
      end
    end

    describe 'Xsweet pipeline step 5' do
      subject                     { XsweetPipeline::ZorbaMapStep.new(process_step: process_step) }
      let(:html_file)            { File.new('spec/fixtures/files/xsweet_5_zorba_map_input.html', 'r') }
      let(:xsl_file)              { File.read('spec/fixtures/files/xsweet_pipeline/zorba-map.xsl') }

      specify do
        test_result(file: html_file, result_path: 'spec/fixtures/files/xsweet_5_zorba_map_result.html')
      end
    end
  end

  describe '#version' do
    specify do
      expect{subject.version}.to_not raise_error
    end
  end
end