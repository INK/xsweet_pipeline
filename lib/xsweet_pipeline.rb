# Modify the module name if you are using this as a template to write your own steps.

module XSweetPipeline
  if defined?(Rails)
    require 'xsweet_pipeline/engine'
  else
    # require any files that need to be exposed to any non-Rails environments here (e.g. test, Sinatra, etc)
    require 'xsweet_pipeline/download_and_execute_xsl_via_saxon'
    require 'xsweet_pipeline/download_and_execute_xsl_via_saxon_on_docx'

    #... anything else
  end
end