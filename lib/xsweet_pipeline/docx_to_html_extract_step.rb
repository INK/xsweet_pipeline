require 'xsweet_pipeline/download_and_execute_xsl_via_saxon_on_docx'

module XsweetPipeline
  class DocxToHtmlExtractStep < DownloadAndExecuteXslViaSaxonOnDocx

    def perform_step(files:, options: {})
      new_hash = options.merge(remote_xsl_uri: remote_xsl_location)
      super(files: files, options: new_hash)
    end

    def remote_xsl_location
      "https://gitlab.coko.foundation/wendell/XSweet/raw/ink-api-publish/docx-html-extract.xsl"
    end

  end
end
