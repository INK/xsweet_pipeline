require 'xsweet_pipeline/download_and_execute_xsl_via_saxon'

module XsweetPipeline
  class ZorbaMapStep < XsweetPipeline::DownloadAndExecuteXslViaSaxon

    def perform_step(files:, options: {})
      new_hash = options.merge(remote_xsl_uri: remote_xsl_location)
      super(files: files, options: new_hash)
    end

    def remote_xsl_location
      "https://gitlab.coko.foundation/wendell/XSweet/raw/ink-api-publish/zorba-map.xsl"
    end

  end
end