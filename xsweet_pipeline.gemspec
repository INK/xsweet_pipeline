# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'xsweet_pipeline/version'

Gem::Specification.new do |spec|
  spec.name           = 'xsweet_pipeline'
  spec.version        = XsweetPipeline::VERSION
  spec.date           = '2016-10-10'
  spec.summary        = "A pipeline step for converting docx to html files within the INK framework"
  spec.description    = "Uses XSL (Saxon) for converting Word files (docx) to HTML using Coko's XSweet XSL sheets"
  spec.authors        = ["Charlie Ablett"]
  spec.email          = 'charlie@coko.foundation'
  spec.homepage       = 'https://gitlab.coko.foundation/INK/xsweet_pipeline'
  spec.license        = 'MIT'

  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths  = %w(lib)

  spec.add_dependency "rubyzip"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "httparty"

  spec.required_ruby_version = '~> 2.2'
  spec.files         = Dir.glob("{lib}/**/*") + %w(./README.md)
end
